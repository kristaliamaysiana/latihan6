package com.binar.binar.repository;

import com.binar.binar.entity.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepo extends PagingAndSortingRepository<Employee, Long> {

    @Query("SELECT e FROM Employee e")
    public List<Employee> getAllEmployee();

    @Query("SELECT e FROM Employee e WHERE e.id = :id")
    public Employee getById(@Param("id") Long id);

    @Query("SELECT e FROM Employee e WHERE e.status = :status")
    public List<Employee> getByStatus(@Param("status") int status);
}

