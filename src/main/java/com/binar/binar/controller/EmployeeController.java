package com.binar.binar.controller;

import com.binar.binar.entity.Employee;
import com.binar.binar.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("v1/employee")
public class EmployeeController {

    @Autowired
    EmployeeService service;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map employees = service.getAll();
        return new ResponseEntity<Map>(employees, HttpStatus.OK);
    }

    @GetMapping("/all/{status}")
    @ResponseBody
    public ResponseEntity<Map> getByStatus(@PathVariable(value = "status") int status) {
        Map employees = service.getByStatus(status);
        return new ResponseEntity<Map>(employees, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Employee newEmployee) {
        Map map = new HashMap();
        Map emp = service.insert(newEmployee);

        map.put("Request = ", newEmployee);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Employee employeeToUpdate) {
        Map map = new HashMap();
        Map emp = service.update(employeeToUpdate);

        map.put("Request = ", employeeToUpdate);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map emp = service.delete(id);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }
}

