package com.binar.binar.service.impl;

import com.binar.binar.entity.Employee;
import com.binar.binar.repository.EmployeeRepo;
import com.binar.binar.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EmployeeImpl implements EmployeeService {

    @Autowired
    public EmployeeRepo repo;

    @Override
    public Map getAll() {
        List<Employee> employees = new ArrayList<Employee>();
        Map map = new HashMap();
        try {
            employees = repo.getAllEmployee();
            map.put("data", employees);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getByStatus(int status) {
        List<Employee> employees = new ArrayList<Employee>();
        Map map = new HashMap();
        try {
            employees = repo.getByStatus(status);
            map.put("data", employees);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map insert(Employee employee) {
        Map map = new HashMap();
        try {
            Employee emp = repo.save(employee);
            map.put("data", emp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee created successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Employee employee) {
        Map map = new HashMap();
        try {
            Employee emp = repo.getById(employee.getId());

            if (emp == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }

            emp.setName(employee.getName());
            emp.setGender(employee.getGender());
            emp.setDob(employee.getDob());
            emp.setAddress(employee.getAddress());
            emp.setStatus(employee.getStatus());
            repo.save(emp);

            map.put("data", emp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee updated successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long employeeId) {
        Map map = new HashMap();
        try {
            Employee emp = repo.getById(employeeId);

            if (emp == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }
            repo.deleteById(emp.getId());

            map.put("statusCode", "200");
            map.put("statusMessage", "Employee deleted successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}

