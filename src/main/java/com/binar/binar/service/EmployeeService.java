package com.binar.binar.service;

import com.binar.binar.entity.Employee;

import java.util.Map;

public interface EmployeeService {

    public Map getAll();

    public Map getByStatus(int status);

    public Map insert(Employee employee);

    public Map update(Employee employee);

    public Map delete(Long employeeId);
}

