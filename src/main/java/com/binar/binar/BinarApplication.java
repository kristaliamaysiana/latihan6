package com.binar.binar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BinarApplication {

	public static void main(String[] args) {
		SpringApplication.run(BinarApplication.class, args);
	}

}
