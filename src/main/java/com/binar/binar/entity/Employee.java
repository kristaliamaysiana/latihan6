package com.binar.binar.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false, length = 45)
    private String name;

    @Column(name = "gender", nullable = false, length = 15)
    private String gender;

    @Column(name = "dob", nullable = false, length = 10)
    private LocalDate dob;

    @Column(name = "address", columnDefinition="TEXT")
    private String address;

    @Column(name = "status", nullable = false, length = 1)
    private int status;

}
